const fName = document.getElementById("fName");
const lName = document.getElementById("lName");
const correctAnswers = {
      "question-1": "wolf",
      "question-2": "boom"
    };

const submitButton = document.querySelector("#submit-button");
const totalScore = document.querySelector("#total-score");

submitButton.addEventListener("click", () => {
  let score = 0;
  
  for (const [questionId, answer] of Object.entries(correctAnswers)) {
    const selectedAnswer = document.querySelector(`#${questionId}`).value;
    const messageElement = document.querySelector(`#${questionId}-message`);

    if (selectedAnswer === answer) {
      messageElement.innerText = "Your Answer is Correct!!! 🥵";
      messageElement.style.color = "green";
      score++;
    } else {
      messageElement.innerText = "Your Answer is Incorrect!!!? 👺";
      messageElement.style.color = "red";
    }
  }

  totalScore.innerText = `Hello ${fName.value} ${lName.value},Your total score is ${score} out of ${Object.keys(correctAnswers).length}.`;

  alert(totalScore.innerText);
});